package com.google.distancematrix.api.model;

import java.time.LocalTime;

public class InputRecord {

	public static final String COLUMN_SEPARATOR = ";";

	private String team;
	private LocalTime arrivalDate;
	private LocalTime finishDate;
	private String latitude;
	private String longitude;
	private String type;

	private boolean ignored;
	private Long timeTraveled = 0L;
	private Long distanceTraveled = 0L;


	public InputRecord(String team, String arrivalDate, String finishDate, String latitude, String longitude, String type) {
		this.team = team;
		this.arrivalDate = LocalTime.parse(arrivalDate);
		this.finishDate = LocalTime.parse(finishDate);
		this.latitude = latitude.replace(',', '.');
		this.longitude = longitude.replace(',', '.');
		this.ignored = true;
		this.type = type;
	}


	public String getTeam() {
		return team;
	}

	public LocalTime getArrivalDate() {
		return arrivalDate;
	}

	public LocalTime getFinishDate() {
		return finishDate;
	}


	public String getLatitude() {
		return latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public String getGoogleUrlRequest(String originLatitude, String originLongitude) {
		return "origins=" + originLatitude + "," + originLongitude
				+ "&destinations=" + latitude + "," + longitude;
	}

	public void setTimeTraveled(Long timeTraveled) {
		this.timeTraveled = timeTraveled;
	}

	public void setDistanceTraveled(Long distanceTraveled) {
		this.distanceTraveled = distanceTraveled;
	}

	public Long getDistanceTraveled() {
		return distanceTraveled;
	}

	public Long getTimeTraveled() {
		return timeTraveled;
	}

	public boolean isIgnored() {
		return ignored;
	}

	public void setIgnored(boolean ignored) {
		this.ignored = ignored;
	}

	public String getType() {
		return type;
	}

	@Override
	public String toString() {
		return "com.google.distancematrix.api.model.InputRecord{" +
				"team='" + team + '\'' +
				", arrivalDate=" + arrivalDate +
				", finishDate=" + finishDate +
				", latitude='" + latitude + '\'' +
				", longitude='" + longitude + '\'' +
				'}';
	}
}
