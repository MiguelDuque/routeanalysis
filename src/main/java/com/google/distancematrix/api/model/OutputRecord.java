package com.google.distancematrix.api.model;


import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.time.LocalTime;

import org.apache.commons.lang3.StringUtils;

public class OutputRecord {

	private static final String COLUMN_SEPARATOR = ";";
	private static final String LINE_SEPARATOR = "\n";

	private String team;
	private LocalTime timeTraveled;
	private double distanceTraveledMeters;
	private LocalTime startTime;
	private LocalTime endTime;
	private int numRecords;
	private int numReal;
	private double productivity;
	private LocalTime timeWorked;
	private double distancePerRecord;
	private double distancePerRealRecord;
	private double timeTravelingPerc;

	private OutputRecord() {
	}

	@Override
	public String toString() {
		return "OutputRecord{" +
				"team='" + team + '\'' +
				", timeTraveled='" + timeTraveled + '\'' +
				", distanceTraveledMeters='" + distanceTraveledMeters + '\'' +
				", startTime=" + startTime +
				", endTime=" + endTime +
				", numRecords=" + numRecords +
				", numReal=" + numReal +
				", productivity=" + productivity +
				", timeWorked=" + timeWorked +
				", distancePerRecord=" + distancePerRecord +
				", distancePerRealRecord=" + distancePerRealRecord +
				'}';
	}

	public static class Builder {
		private String team;
		private LocalTime timeTraveled;
		private double distanceTraveledMeters;
		private LocalTime startTime;
		private LocalTime endTime;
		private int numRecords;
		private int numReal;
		private double productivity;
		private LocalTime timeWorked;
		private double distancePerRecord;
		private double distancePerRealRecord;
		private double timeTravelingPerc;


		public Builder(String team) {
			this.team = team;
		}

		public Builder withTimeTraveled(LocalTime timeTraveled) {
			this.timeTraveled = timeTraveled;
			return this;
		}

		public Builder withDistanceTraveled(double distanceTraveledMeters) {
			this.distanceTraveledMeters = distanceTraveledMeters;
			return this;
		}

		public Builder withStartTime(LocalTime startTime) {
			this.startTime = startTime;
			return this;
		}

		public Builder withEndTime(LocalTime endTime) {
			this.endTime = endTime;
			return this;
		}

		public Builder withNumRecords(int numRecords) {
			this.numRecords = numRecords;
			return this;
		}

		public Builder withNumReal(int numReal) {
			this.numReal = numReal;
			return this;
		}

		public Builder withProductivity(double productivity) {
			this.productivity = productivity;
			return this;
		}

		public Builder withTimeWorked(LocalTime timeWorked) {
			this.timeWorked = timeWorked;
			return this;
		}

		public Builder withDistancePerRecord(double distancePerRecord) {
			this.distancePerRecord = distancePerRecord;
			return this;
		}

		public Builder withDistancePerRealRecord(double distancePerRealRecord) {
			this.distancePerRealRecord = distancePerRealRecord;
			return this;
		}

		public Builder withTimeTravelingPerc(double timeTravelingPerc) {
			this.timeTravelingPerc = timeTravelingPerc;
			return this;
		}

		public OutputRecord build() {
			OutputRecord outputRecord = new OutputRecord();
			outputRecord.team = this.team;
			outputRecord.timeTraveled = this.timeTraveled;
			outputRecord.distanceTraveledMeters = this.distanceTraveledMeters;
			outputRecord.startTime = this.startTime;
			outputRecord.endTime = this.endTime;
			outputRecord.numRecords = this.numRecords;
			outputRecord.numReal = this.numReal;
			outputRecord.productivity = this.productivity;
			outputRecord.timeWorked = this.timeWorked;
			outputRecord.distancePerRecord = this.distancePerRecord;
			outputRecord.distancePerRealRecord = this.distancePerRealRecord;
			outputRecord.timeTravelingPerc = this.timeTravelingPerc;

			return outputRecord;
		}
	}

	public static String getHeaderOutput() {
		return String.join(
				COLUMN_SEPARATOR,

				"team",
				"timeTraveled",
				"distanceTraveled",
				"startTime",
				"endTime",
				"numRecords",
				"numReal",
				"productivity",
				"timeWorked",
				"distancePerRecord",
				"distancePerRealRecord",
				"timeTravelingPerc"
				)

				+ LINE_SEPARATOR;
	}

	public static String getOutputInformation(OutputRecord outputRecord) {
		return String.join(
				COLUMN_SEPARATOR,

				outputRecord.team,
				outputRecord.timeTraveled.toString(),
				formatNumberOutput(outputRecord.distanceTraveledMeters),
				outputRecord.startTime.toString(),
				outputRecord.endTime.toString(),
				Integer.toString(outputRecord.numRecords),
				Integer.toString(outputRecord.numReal),
				formatNumberOutput(outputRecord.productivity),
				outputRecord.timeWorked.toString(),
				formatNumberOutput(outputRecord.distancePerRecord),
				formatNumberOutput(outputRecord.distancePerRealRecord),
				formatNumberOutput(outputRecord.timeTravelingPerc * 100)
				)

				+ LINE_SEPARATOR;
	}

	private static String formatNumberOutput(double value) {
		DecimalFormat decimalFormat = new DecimalFormat("#.###");
		decimalFormat.setRoundingMode(RoundingMode.CEILING);

		return StringUtils.replace(decimalFormat.format(value), ".", ",");
	}
}
