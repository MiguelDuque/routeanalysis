package com.google.distancematrix.api.service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.distancematrix.api.model.InputRecord;

public class GoogleApi {

	private static final Logger LOGGER = LoggerFactory.getLogger(GoogleApi.class);
	private static final ObjectMapper mapper = new ObjectMapper();

	private static final String GOOGLE_API_URL = "https://maps.googleapis.com/maps/api/distancematrix/json?";

	private final int googleApiKeysMaxUse;
	private final String[] googleApiKeys;

	private int googleApiCurrentKey;
	private int numRequests;

	public GoogleApi(int googleApiKeysMaxUse, String[] googleApiKeys) {
		this.numRequests = 0;
		this.googleApiCurrentKey = 0;
		this.googleApiKeysMaxUse = googleApiKeysMaxUse;
		this.googleApiKeys = googleApiKeys;
	}


	public void processRecord(String pastLongitude, String pastLatitude, InputRecord inputRecord) throws Exception {
		String request;
		try {
			String urlKeyParameter = "&key=" + googleApiKeys[googleApiCurrentKey];
			request = GOOGLE_API_URL + inputRecord.getGoogleUrlRequest(pastLatitude, pastLongitude) + urlKeyParameter;

			if (numRequests++%googleApiKeysMaxUse == 0 && googleApiKeysMaxUse > 0) {
				googleApiCurrentKey++;
			}
		} catch (IndexOutOfBoundsException e) {
			LOGGER.error("New Google API key required");
			throw e;
		}
		JsonNode travelInformation = requestTravelInformation(request);
		validateTravelInformation(travelInformation, inputRecord, request);
	}

	private JsonNode requestTravelInformation(String url) throws Exception {

		try {
			URL obj = new URL(url);

			HttpURLConnection con = (HttpURLConnection) obj.openConnection();

			int responseCode = con.getResponseCode();
			if(responseCode != 200) {
				throw new Exception("Response code " + responseCode);
			}

			BufferedReader in = new BufferedReader( new InputStreamReader(con.getInputStream()));
			StringBuffer response = new StringBuffer();

			String inputLine;
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();

			return mapper.readTree(response.toString());

		} catch (Exception e) {
			LOGGER.error("Error accessing the following Google API Url: {}", url);
			throw e;
		}
	}

	private void validateTravelInformation(JsonNode travelInformation, InputRecord inputRecord, String request) {
		String GOOGLE_RESPONSE_DISTANCE_KEY = "/rows/0/elements/0/distance/value";
		String GOOGLE_RESPONSE_DURATION_KEY = "/rows/0/elements/0/duration/value";

		JsonNode distanceTraveledNode = travelInformation.at(GOOGLE_RESPONSE_DISTANCE_KEY);
		JsonNode timeTraveledNode = travelInformation.at(GOOGLE_RESPONSE_DURATION_KEY);

		if(distanceTraveledNode.isNull() || distanceTraveledNode.isMissingNode()
				|| timeTraveledNode.isNull() || timeTraveledNode.isMissingNode()) {

			inputRecord.setIgnored(true);
			throw new RuntimeException("Not possible to get information for the following request:\n"
					+ request + "\n"
					+ inputRecord.toString());
		}

		inputRecord.setDistanceTraveled((long) distanceTraveledNode.intValue());
		inputRecord.setTimeTraveled((long) timeTraveledNode.intValue());
	}
}
