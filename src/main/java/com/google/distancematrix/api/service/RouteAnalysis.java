package com.google.distancematrix.api.service;


import static java.time.temporal.ChronoUnit.NANOS;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.distancematrix.api.model.InputRecord;
import com.google.distancematrix.api.model.OutputRecord;

public class RouteAnalysis {


	private static final Logger LOGGER = LoggerFactory.getLogger(RouteAnalysis.class);
	private static final String TYPE_REAL = "REAL";
	private static final int MESSAGE_CHECKPOINT_COUNT = 50; // Control message every X records

	private final GoogleApi googleApi;


	public RouteAnalysis(int googleApiKeysMaxUse, String[] googleApiKeys) {
		this.googleApi = new GoogleApi(googleApiKeysMaxUse, googleApiKeys);
	}


	public List<InputRecord> readFile(Path input) throws IOException {
		List<InputRecord> inputRecords = new ArrayList<>();
		LOGGER.info("Started reading file");

		try (Stream<String> stream = Files.lines(input)) {
			stream
					.skip(1) // Skip header
					.forEach(line -> {
						String[] split = line.split(InputRecord.COLUMN_SEPARATOR, -1);
						inputRecords.add(new InputRecord(split[0], split[1], split[2], split[3], split[4], split[5]));
					});
		} catch (IOException e) {
			LOGGER.error("Error reading file", e);
			throw e;
		}

		LOGGER.info("Finished reading file. Successfully read {} records", inputRecords.size());

		return inputRecords;
	}


	public Map<String, List<InputRecord>> calculateTeamIndividualInformation(List<InputRecord> inputRecords) throws Exception {
		LOGGER.info("Started processing individual records");
		Map<String, List<InputRecord>> recordsByTeam = inputRecords.stream().collect(Collectors.groupingBy(InputRecord::getTeam));

		int numProcessedRecords = 0;
		for(String team : recordsByTeam.keySet()) {
			List<InputRecord> teamInputRecords = recordsByTeam.get(team);
			teamInputRecords.sort(Comparator.comparing(InputRecord::getArrivalDate));
			numProcessedRecords = calculateTeamInformation(teamInputRecords, numProcessedRecords);
		}
		LOGGER.info("Finished processing individual records, {} teams identified", recordsByTeam.size());
		return recordsByTeam;
	}


	private int calculateTeamInformation(List<InputRecord> teamInputRecords, int numProcessedRecords) throws Exception {
		String pastLongitude = "";
		String pastLatitude = "";

		for(InputRecord inputRecord : teamInputRecords) {
			numProcessedRecords++;

			if(isValidCoordinates(inputRecord.getLatitude(), inputRecord.getLongitude())) {
				if(isValidCoordinates(pastLatitude, pastLongitude)) {
					googleApi.processRecord(pastLongitude, pastLatitude, inputRecord);
					inputRecord.setIgnored(false);
				}
				pastLatitude = inputRecord.getLatitude();
				pastLongitude = inputRecord.getLongitude();
			}

			if(numProcessedRecords % MESSAGE_CHECKPOINT_COUNT == 0) {
				LOGGER.info("{} records processed", numProcessedRecords);
			}
		}

		return numProcessedRecords;
	}


	private boolean isValidCoordinates(String latitude, String longitude) {
		if(StringUtils.isEmpty(latitude) || StringUtils.isEmpty(longitude)) {
			return false;
		}

		try {
			double latitudeNumber = Double.parseDouble(latitude);
			double longitudeNumber = Double.parseDouble(longitude);

			return latitudeNumber > 36.721188 && latitudeNumber < 42.302834
					&& longitudeNumber > -9.768462 && longitudeNumber < -6.049244;
		} catch (Exception e) {
			LOGGER.warn("Coordinates with invalid format ignored ({}, {})", latitude, longitude);
			return false;
		}
	}


	public List<OutputRecord> getTeamsOutputInformation(Map<String, List<InputRecord>> informationByTeam) {
		LOGGER.info("Started calculating teams information");
		List<OutputRecord> teamsInfo = informationByTeam.entrySet().stream()
				.map(teamEntry -> calculateTeamOutputInfo(teamEntry.getKey(), teamEntry.getValue()))
				.collect(Collectors.toList());
		LOGGER.info("Finished calculating teams information");
		return teamsInfo;
	}


	private OutputRecord calculateTeamOutputInfo(String team, List<InputRecord> teamInputRecords) {
		long timeTraveledSec = 0;
		double distanceTraveled = 0;
		int countReal = 0;

		for(InputRecord r : teamInputRecords) {
			timeTraveledSec += r.getTimeTraveled();
			distanceTraveled += r.getDistanceTraveled();
			if(r.getType().equals(TYPE_REAL)){
				countReal++;
			}
		}


		int numRecords = teamInputRecords.size();
		LocalTime startTime = teamInputRecords.get(0).getArrivalDate();
		LocalTime endTime = teamInputRecords.get(numRecords - 1).getFinishDate();
		LocalTime timeWorked = endTime.minus(startTime.toNanoOfDay(), NANOS);
		LocalTime timeTraveled = LocalTime.ofSecondOfDay(timeTraveledSec);


		return new OutputRecord.Builder(team)
				.withTimeTraveled(timeTraveled)
				.withDistanceTraveled(distanceTraveled)
				.withStartTime(startTime)
				.withEndTime(endTime)
				.withNumRecords(numRecords)
				.withNumReal(countReal)
				.withProductivity(((double) countReal)/ numRecords)
				.withTimeWorked(timeWorked)
				.withDistancePerRecord(distanceTraveled/numRecords)
				.withDistancePerRealRecord((countReal == 0) ? 0 : distanceTraveled/countReal)
				.withTimeTravelingPerc(((double) timeTraveled.toNanoOfDay()) / timeWorked.toNanoOfDay())
				.build();
	}


	public void createOutputFile(Path output, List<OutputRecord> teamsOutputInformation) throws IOException {
		LOGGER.info("Started writing output file");
		try (FileWriter fileWriter = new FileWriter(output.toFile())) {
			fileWriter.append(OutputRecord.getHeaderOutput());
			teamsOutputInformation
					.forEach(outputRecord -> {
						try {
							fileWriter.write(OutputRecord.getOutputInformation(outputRecord));
						} catch (Exception e) {
							LOGGER.error("Error writing the following record in the file: \n {}", outputRecord.toString(), e);
						}
					});
		} catch (IOException e) {
			LOGGER.error("Error creating output file");
			throw e;
		}
		LOGGER.info("Finished writing output file");
	}
}