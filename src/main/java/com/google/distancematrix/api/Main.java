package com.google.distancematrix.api;

import java.nio.file.Paths;
import java.util.List;
import java.util.Map;

import com.google.distancematrix.api.model.InputRecord;
import com.google.distancematrix.api.model.OutputRecord;
import com.google.distancematrix.api.service.RouteAnalysis;

public class Main {
	private static final String INPUT_FOLDER = "input";
	private static final String INPUT_FILE = "Example-Input.csv";

	private static final String OUTPUT_FOLDER = "output";
	private static final String OUTPUT_FILE = "Output.csv";

	private static final String[] GOOGLE_API_KEYS = {
			"AIzaSyBNgzEEdKZmArHck4DAPkfSsLI4S6FNIoc"
	//		, "key2"
	//		, "key3"
			};
	private static final int GOOGLE_API_KEYS_MAX_USE = -1; // limit of requests per key. -1 if no limit


    public static void main(String[] args) throws Exception {
    	
    	RouteAnalysis routeAnalysis = new RouteAnalysis(GOOGLE_API_KEYS_MAX_USE, GOOGLE_API_KEYS);
    	

    	List<InputRecord> allInputRecords =
			    routeAnalysis.readFile(Paths.get(INPUT_FOLDER).resolve(INPUT_FILE));

    	Map<String, List<InputRecord>> informationByTeam =
			    routeAnalysis.calculateTeamIndividualInformation(allInputRecords);

    	List<OutputRecord> teamsOutputInformation =
			    routeAnalysis.getTeamsOutputInformation(informationByTeam);

    	routeAnalysis.createOutputFile(Paths.get(OUTPUT_FOLDER).resolve(OUTPUT_FILE), teamsOutputInformation);
    }
}
